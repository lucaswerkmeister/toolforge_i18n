:gitlab_url: https://gitlab.wikimedia.org/lucaswerkmeister/toolforge_i18n/-/blob/main/docs/api.rst

API Reference
=============

This page contains the documentation of all the members exported by toolforge_i18n.
Not all of them are useful to tools; some should arguably not really be used by any tools.
Members are also not listed in any useful order.
For these reasons, it doesn’t really make sense to read through this document front to back.
Please follow the other documentation instead,
and consult this page only if you are interested in details on a particular member.

.. automodule:: toolforge_i18n
    :members:
    :imported-members:
