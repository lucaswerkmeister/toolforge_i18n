:gitlab_url: https://gitlab.wikimedia.org/lucaswerkmeister/toolforge_i18n/-/blob/main/docs/flask/index.rst

Using toolforge_i18n in a Flask tool
====================================

This section documents how to use toolforge_i18n in a tool that uses the `Flask <https://flask.palletsprojects.com/>`_ framework.
You probably want to start at :doc:`setup`,
then continue with :doc:`development`.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   setup
   development
